# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

 let
   home-manager = builtins.fetchGit {
     url = "https://github.com/rycee/home-manager.git";
     rev = "612afee126c664121cb8bc071b999696513df808"; # CHANGEME 
     ref = "release-20.09";
   };

   neoqwertz = pkgs.fetchFromGitHub {
     owner  = "andreas-hofmann";
     repo   = "neoqwertz";
     rev    = "7ec7db6b42c6c1b6ad17bbdff0966dda39cfc962";
     sha256 = "1sjyzhyc07lzkas71s00rhcyvmhvlvg84nqp2hq72yazy2m6m3nq";
    };

    dwmScripts = import dotfiles/dwmScripts.nix { inherit pkgs; };
 in
{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (import "${home-manager}/nixos")
      ./dotfiles
    ];

  # Use the GRUB 2 boot loader.
  boot.kernelPackages = pkgs.linuxPackages_zen;
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only

  networking.hostName = "nixos-vm"; # Define your hostname.

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  console = {
    #   font = "Lat2-Terminus16";
    keyMap = "de";
  };


  security.sudo.wheelNeedsPassword = false;

  # Configure keymap in X11
  services.xserver = {
    enable = true;
    resolutions = [ { x = 1920; y = 1080; } ];
    layout = "de";
    xkbVariant = "nodeadkeys";
    windowManager.dwm.enable = true;
    desktopManager = {
      xterm.enable = false;
    };
    displayManager = {
      defaultSession = "none+dwm";
      autoLogin.enable = true;
      autoLogin.user = "jonas";
      sessionCommands = ''
        ${pkgs.xorg.xkbcomp}/bin/xkbcomp ${neoqwertz}/neoqwertz_full_definitions.xkb $DISPLAY
        ${dwmScripts.statusBarScript} &
      '';
    };
  };

  # enable support for graphical 32-Bit applications
  hardware.opengl.driSupport32Bit = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jonas = {
    isNormalUser = true;
    home = "/home/jonas";
    password = "123";
    extraGroups = [ "wheel" "audio" ]; # Enable ‘sudo’ for the user. And allow audio playback
    shell = pkgs.zsh;
  };


  nixpkgs.config.pulseaudio = true;
  nixpkgs.overlays = [
    (import /etc/nixos/overlays/st.nix)
    (import /etc/nixos/overlays/arc-theme.nix)
    (import /etc/nixos/overlays/dwm.nix)
    (import /etc/nixos/overlays/sxiv.nix)
    (import /etc/nixos/overlays/nvim)
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    neovim
    st
    dmenu
    sxiv
    pavucontrol
    # -------
    rnix-lsp # for coc.vim nix LSP support
    nixpkgs-fmt # for coc.vim nix LSP support
    nodejs # for coc.nvim
    # -------
    wget
    jq # commandline json parser
    lesspipe
    rofi
    exa
    fd
    git
    gitAndTools.delta # fancy diffs
    fzf
    gotop
    neofetch
    # -------
    firefox
    (xfce.thunar.override { thunarPlugins = [xfce.thunar-archive-plugin xfce.thunar-volman]; })
    lxappearance
    arc-theme
    arc-icon-theme
    # -------
    zsh
    zsh-powerlevel10k
    nix-zsh-completions
    fish
  ];

  fonts.fonts = with pkgs; [
    siji
    envypn-font
    # jetbrains-mono
    (nerdfonts.override {
      fonts = [ "JetBrainsMono" ];
    })
  ];

  environment.extraInit = ''
    # GTK3: remove local user overrides (for determinisim, causes hard to find bugs)
    rm -f ~/.config/gtk-3.0/settings.ini

    # GTK3: add /etc/xdg/gtk-3.0 to search path for settings.ini
    # We use /etc/xdg/gtk-3.0/settings.ini to set the icon and theme name for GTK 3
    export XDG_CONFIG_DIRS="/etc/xdg:$XDG_CONFIG_DIRS"

    # GTK3: add theme to search path for themes
    export XDG_DATA_DIRS="${pkgs.arc-theme}/share:${pkgs.arc-icon-theme}/share:$XDG_DATA_DIRS"

    # GTK2 theme + icon theme
    export GTK2_RC_FILES=${pkgs.writeText "iconrc" ''gtk-icon-theme-name="Arc"''}:${pkgs.arc-theme}/share/themes/Arc-Dark/gtk-2.0/gtkrc:$GTK2_RC_FILES

    # these are the defaults, but some applications are buggy so we set them
    # here anyway
    export XDG_CONFIG_HOME=$HOME/.config
    export XDG_DATA_HOME=$HOME/.local/share
    export XDG_CACHE_HOME=$HOME/.cache

  '';

  # GTK3 global theme (widget and icon theme)
  environment.etc."xdg/gtk-3.0/settings.ini" = {
    text = ''
      [Settings]
      gtk-application-prefere-dark-theme=1
      gtk-enable-animations=1
      gtk-icon-theme-name=Arc
      gtk-theme-name=Arc-Dark
      gtk-font-name=JetBrainsMono Nerd Font 10
      gtk-button-images=1
      gtk-menu-images=1
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle=hintslight
      gtk-xft-rgba=rgb
      gtk-primary-button-warps-slider = false
    '';
    mode = "444";
  };

  environment.variables.EDITOR = "nvim";
  programs.zsh.enable = true;
  programs.zsh.promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
  environment.pathsToLink = [ "/share/zsh" ]; # for completions

  programs.less.enable = true;
  programs.less.lessopen = "|${pkgs.lesspipe}/bin/lesspipe.sh %s";

  programs.file-roller.enable = true; # for thunar

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
