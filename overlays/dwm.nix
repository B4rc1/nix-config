self: super:
{
  dwm = super.dwm.overrideAttrs (old: {
  	src = super.fetchFromGitLab {
	  owner = "B4rc1";
	  repo = "dwm";
	  rev = "a290be1a85f01cc83403b97f62cd1b5a3d0d0022";
	sha256 = "0lc88000jfxf79g6m512hj8b8x18sav05mbhz8ig1zdjldg6mdp5";
	};

	buildInputs = (old.buildInputs ++ [ super.harfbuzz ]);
  });
}
