self: super:
{
  sxiv = super.sxiv.overrideAttrs (old: {
  	src = super.fetchFromGitLab {
	  owner = "B4rc1";
	  repo = "sxiv";
	  rev = "17eab477cb1b783bfa51c637d213d9bfded9af66";
	  sha256 = "1b4v5gddx388gyv3r9yscncbffq5lx9srqg79lwlvd08q7md8qgl";
	};
  });
}
