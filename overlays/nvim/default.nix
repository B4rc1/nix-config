self: super:
 let
   my_plugins = import ./plugins.nix { inherit (super) vimUtils fetchFromGitLab ;};
   my_vimrc   = import ./vimrc;
 in
{
 neovim = super.neovim.override {
   vimAlias = true;
    configure = {
      customRC = my_vimrc.config;
      plug.plugins = with self.pkgs.vimPlugins; with my_plugins; [ vim-airline vim-nix coc-highlight coc-nvim coc-pairs fugitive vim-devicons
                                                  vim-deep-space fzfWrapper fzf-vim vim-commentary vim-devicons
                                                  vimagit gv-vim vim-peekaboo vim-sneak
                                                ];
                                                  # vim-fish ];
    };
  };
}
