{
  config = ''

" ▌     ▌ ▘   ▌▘
" ▙▘█▌▌▌▛▌▌▛▌▛▌▌▛▌▛▌▛▘
" ▛▖▙▖▙▌▙▌▌▌▌▙▌▌▌▌▙▌▄▌
"     ▄▌          ▄▌
"=====================
" Unmap Space and set it to leader
nnoremap <Space> <NOP>
" iunmap <ESC> THIS SHIT DOES NOT WORK TODO: FIX
let mapleader = "\<Space>"

" =============
" Normal Mode
" =============
" Window Movement
" nnoremap <Leader>wl <C-W><C-L>
" nnoremap <Leader>wh <C-W><C-h>
" nnoremap <Leader>wj <C-W><C-j>
" nnoremap <Leader>wk <C-W><C-k>
nnoremap <Leader>w <C-W>

" File Control
" Save with SPC f s
nnoremap <Leader>fs :w<CR>
nnoremap <Leader>fr :History<CR>

" Splits
nnoremap <Leader>wv :vsplit<CR>
nnoremap <Leader>ws :split<CR>
nnoremap <Leader>wd :close<CR>

" Buffer Control
nnoremap <Leader>bn :bn<CR>
nnoremap <Leader>bp :b#<CR>
nnoremap <Leader>bb :Buffers<CR>
nnoremap <Leader>bl :Lines<CR>
nnoremap <Leader>bd :bd<CR>

" Plug control
nnoremap <Leader>pi :PlugInstall<CR>
nnoremap <Leader>pu :PlugUpdate<CR>

" vim control
nnoremap <Leader>qq :qa<CR>

" PWD control
nnoremap <Leader>cd :cd %:h<CR>
nnoremap <Leader>ch :cd ~<CR>

" Quickfix window, resize cuz Goyo
nnoremap <Leader>qc :cclose<CR>:resize<CR>
nnoremap <Leader>qo :copen<CR>

" help bindings
nnoremap <Leader>hb :Maps<CR>

" Space Space for FZF-Files
nnoremap <Leader><Leader> :Files<CR>

" gv.vim bindings
nnoremap <Leader>gv :GV<CR>


" =============
" Insert Mode
" =============
" Dont hit escape! hit jk
imap jk <ESC>


" =============
" Visual Mode
" =============

vnoremap <Leader>gv :GV?<CR>

" ==============
" Terminal Mode
" =============
tnoremap jk <C-\><C-n>
tnoremap <Esc> <C-\><C-n>
    '';
}
