{
  config = ''
" ▄▖          ▜   ▄▖  ▗ ▗ ▘
" ▌ █▌▛▌█▌▛▘▀▌▐   ▚ █▌▜▘▜▘▌▛▌▛▌▛▘
" ▙▌▙▖▌▌▙▖▌ █▌▐▖  ▄▌▙▖▐▖▐▖▌▌▌▙▌▄▌
"                            ▄▌
"================================
" Line numbers
set number
" horizontal cursor line
set cursorline
" system clipboard as default register
set clipboard+=unnamedplus
" make search only case sensitive if a upper case character is in search
" string
set ignorecase smartcase
" Autoindenting
set smartindent smarttab
" 4 spaces tab
set tabstop=4
set shiftwidth=4
" Hybrid Line Numbers
set number relativenumber
" Show trailing spaces and tabs
set list
" relative numbers and relative numbers when Focus, otherwise absolute ones
autocmd WinEnter * set relativenumber cursorline
autocmd WinLeave * set norelativenumber nocursorline
" Ask before hiding buffers with modified change
set confirm
" Keep 1 lines after cursor on screen
set scrolloff=1
" no backups no writebackups and swapfiles, cuz CoC does not like it and neither do i
set nobackup nowritebackup noswapfile
" Display title
set title
" autoCD into file directory
" set autochdir

" persistend UNDO
set undodir="~/.cache/nvim/undodir"
set undofile

" start searching before pressing enter
set incsearch

" ============= airline =============
" Tabline extension
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0  " needed for tab_min_count
let g:airline#extensions#tabline#tab_min_count = 2 " Hide tabline when only one buffer
let g:airline#extensions#tabline#formatter = "unique_tail" " Only show filename not path

let g:airline_powerline_fonts = 1 "enable powerline fonts

" enable symbol customisation
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif


let g:airline_left_sep = ""
let g:airline_left_alt_sep = '|'
let g:airline_right_sep = ""
let g:airline_right_alt_sep = '|' " no seperators
let g:airline_symbols.linenr = "☰  " " more spaceing because the font is wired
let g:airline_symbols.branch = "⎇ "  " here aswell

let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#wordcount#enabled = 1


    '';
}
