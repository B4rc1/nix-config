let
  general     = import ./generalSettings.nix;
  theme       = import ./theme.nix;
  keybindings = import ./keybindings.nix;
  coc         = import ./coc.nix;
in
{
  config = ''
    ${general.config}
    ${theme.config}
    ${keybindings.config}
    ${coc.config}
'';
}
