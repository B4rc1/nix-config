{
  config = ''
" ▄▖▌
" ▐ ▛▌█▌▛▛▌█▌
" ▐ ▌▌▙▖▌▌▌▙▖
"============
"
" Use all the colors
set background=dark
set termguicolors
colorscheme deep-space

" Dont show -- Insert -- at bottom
set noshowmode

let g:airline_theme='deep_space'
    '';
}
