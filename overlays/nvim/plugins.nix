{ vimUtils, fetchFromGitLab }:
{
  vim-deep-space = vimUtils.buildVimPlugin {
    name = "vim-deep-space";
    src = fetchFromGitLab {
      owner = "B4rc1";
      repo = "vim-deep-space";
      rev = "ad7cc3efdf9518a2c1e65c6974b3a15535d075a5";
      sha256 = "0llnpagxilm9xhns4asikw84gla15z6mfvp6azx5lzm2fmxnbbwk";
    };
  };
}
