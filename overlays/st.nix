self: super:
{
  st = (super.st.overrideAttrs (old: {
  	src = super.fetchFromGitLab {
	  owner = "B4rc1";
	  repo = "st";
	  rev = "0699d29b3cf7b185b637ec2af56f0025efbf9caf";
	  sha256 = "0jsa27iwvh7f9ilr60swdxfgww5szjy5rxlbsn2zj923k5cr98g7";
	};
  })).override { extraLibs = [ super.harfbuzz ]; };
}
