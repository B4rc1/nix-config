self: super:
{
  arc-theme = super.arc-theme.overrideAttrs (old: {
  # src = super.fetchFromGitHub {
  #   owner = "jnsh";
  #   repo = "arc-theme";
  #   rev = "20201013";
  #   sha256 = "1x2l1mwjx68dwf3jb1i90c1q8nqsl1wf2zggcn8im6590k5yv39s";
  # };
  	src = super.fetchFromGitLab {
	  owner = "B4rc1";
	  repo = "arc-theme";
	  rev = "85517b6c92fadc79fef6aae40fc464acd3db7bef";
	  sha256 = "0b7lv0cjfg39hh7c0xi0xz47221mhz9m49x8s65nib2qlwi7cpw9";
	};
  });
}
