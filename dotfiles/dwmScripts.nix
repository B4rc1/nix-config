{ pkgs, ...}:
{
  statusBarScript = pkgs.writeShellScript "dwm-status" ''
LANG=de_DE

while [ $(date +'%S') -ne "00" ]; do 
	sleep 0.2s
done

echo "[bar] Starting to Display bar..."

while true; do
	time=$(date +'  %a %b %d  %H:%M |')
	xsetroot -name "$time"
	sleep 60s
done
  '';
}
