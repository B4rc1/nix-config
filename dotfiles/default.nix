{ pkgs, ... }:

let
  firefoxcss = import ./firefoxcss.nix;
  rofi = import ./rofi.nix { inherit pkgs; };
in
{
  home-manager.users.jonas = {
    programs.fzf.enable = true;

    xdg.configFile."rofi/config.rasi".text = ''
      configuration {
          modi: "window,drun";
          show-icons: true;
          icon-theme: "Arc";
          theme: "${rofi.theme}";
      }
              '';

    programs.git.enable = true;
    programs.git.delta.enable = true;
    programs.git.delta.options = { line-numbers = true; };

    programs.firefox.enable = true;
    programs.firefox.extensions = with pkgs.nur.repos.rycee.firefox-addons; [
          ublock-origin
          tridactyl
          keepassxc-browser
          privacy-badger
          stylus
          https-everywhere
    ];

    programs.firefox.profiles = {
      home = {
        id = 0;
        isDefault = true;
        settings = {
          "app.update.auto" = false;
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true; # allow css customisation
          "browser.startup.page" = 3; # restore tabs on close
          "browser.sessionstore.warnOnQuit" = false; # dont warn on quit
          "privacy.clearOnShutdown.sessions" = false; # remember sessions through shutdowns
          "browser.sessionstore.restore_on_demand" = false; # restore all tabs on startup
          "browser.download.useDownloadDir" = false; # Ask for every Download where to save to
          "clipboard.plainTextOnly" = true; # dont copy typesetting information
          "media.videocontrols.picture-in-picture.enabled" = false; # disable media controls
          "media.videocontrols.picture-in-picture.video-toggle.enabled" = false; # disable media controls
          # deactivate useless shit
          "beacon.enabled" = false;
          "extensions.pocket.enabled" = false;
          "extensions.screenshots.disabled" = true;
          "extensions.blocklist.enabled" = false;
          "datareporting.policy.dataSubmissionEnabled" = false;
          "app.normandy.enabled"  = false;
          "extensions.webextensions.restrictedDomains" = "";
          "narrate.enabled" = false; # disable narrator
        };
        userChrome = firefoxcss.userChrome;
        userContent = firefoxcss.userContent;
      };

    };

    programs.zsh = {
      enable = true;
      enableAutosuggestions = true;
      history.extended = true;
      defaultKeymap = "emacs";

      shellAliases = {
        v = "nvim";
        ls = "exa";
        l = "exa -la";
        ll = "exa -l";
        econf = "cd /etc/nixos; sudo -E nvim configuration.nix";
      };

      initExtraBeforeCompInit = ''
        # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
        # Initialization code that may require console input (password prompts, [y/n]
        # confirmations, etc.) must go above this block; everything else may go below.
        if [[ -r "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh" ]]; then
          source "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh"
        fi

      '';

      initExtra = ''
        zstyle ':completion:*' menu select
        zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

        bindkey "^[[1;5C" forward-word
        bindkey "^[[1;5D" backward-word
        # unmap Ctrl +x
        bindkey -per "^X"
        bindkey "^X" kill-word

        # Complete with ctrl + space
        bindkey "^ " autosuggest-accept

        [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
        '';

      plugins = [
        {
          # will source zsh-autosuggestions.plugin.zsh
          name = "zsh-autosuggestions";
          src = pkgs.fetchFromGitHub {
            owner = "zsh-users";
            repo = "zsh-autosuggestions";
            rev = "v0.6.4";
            sha256 = "0h52p2waggzfshvy1wvhj4hf06fmzd44bv6j18k3l9rcx6aixzn6";
          };
        }
        {
          name = "zsh-syntax-highlighting";
          src = pkgs.fetchFromGitHub {
            owner = "zsh-users";
            repo = "zsh-syntax-highlighting";
            rev = "0.7.1";
            sha256 = "03r6hpb5fy4yaakqm3lbf4xcvd408r44jgpv4lnzl9asp4sb9qc0";
          };
        }
      ];
    };
  };
}
